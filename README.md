# Installation Instructions:

1. Clone the Repository:
Clone the repository containing the script to your local machine:
`git clone https://github.com/your_username/your_repository.git`

2. Install Dependencies:
Navigate to the directory where the script is located and install the required dependencies using pip:
`pip install -r requirements.txt`

3. Prepare Configuration File:
Create a JSON configuration file (config.json) with the following structure:
```json
{
    "aws_access_key_id": "YOUR_ACCESS_KEY",
    "aws_secret_access_key": "YOUR_SECRET_KEY",
    "destination_folder": "your-folder/",
    "bucket_name": "kayhan-t10"
}
```

Replace `YOUR_ACCESS_KEY`, `YOUR_SECRET_KEY` with your AWS credentials, and `destination_folder` with Kayhan-provided destination folder in the S3 bucket.

4. Run the script:
Execute the script from the command line, providing the path to the file you want to upload and the path to the configuration file:
```bash
python kayhan_gnss_uploader.py /path/to/local/gnss_799123456_2023_02_28_12_33_44Z.json --config-file config.json
```

Replace `/path/to/local/gnss_799123456_2023_02_28_12_33_44Z.json` with the path to the file you want to upload, and replace `config.json` with the name of your configuration file.
