import os, sys
import argparse
import boto3
import json
import re
from datetime import datetime

class S3Uploader:
    def __init__(self, aws_access_key_id, aws_secret_access_key, destination_folder, bucket_name):
        self.s3 = boto3.client('s3', aws_access_key_id=aws_access_key_id, aws_secret_access_key=aws_secret_access_key)
        self.destination_folder = destination_folder

    def upload_file(self, file_path, bucket_name):
        file_name = os.path.basename(file_path)
        # Validate file name format
        if not self.validate_file_name(file_name):
            raise ValueError("File name does not match the required format.")

        # Extract NORAD ID from the file name
        norad_id = self.get_norad(file_name)

        # Upload file to S3
        s3_key = os.path.join(self.destination_folder, norad_id, file_name)
        
        self.s3.upload_file(file_path, bucket_name, s3_key)

    def validate_file_name(self, file_name):
        pattern = r"gnss_\d{9}_\d{4}_\d{2}_\d{2}_\d{2}_\d{2}_\d{2}Z\.(json|txt)"
        return bool(re.match(pattern, file_name))
    
    def get_norad(self, file_name):
        parts = file_name.split('_')
        if len(parts) >= 2:
            return parts[1]
        raise ValueError("File name does not match the required format")
        
        print("Error: File name does not match the required format.", file=sys.stderr)
        return

def main():
    parser = argparse.ArgumentParser(description="Upload file to S3")
    parser.add_argument("file_path", help="Path to the file to upload")
    parser.add_argument("--config-file", required=True, help="Path to the config file")
    args = parser.parse_args()

    # Read config file
    with open(args.config_file, 'r') as f:
        config = json.load(f)

    # Extract credentials and destination folder from config
    aws_access_key_id = config.get('aws_access_key_id')
    aws_secret_access_key = config.get('aws_secret_access_key')
    destination_folder = config.get('destination_folder')
    bucket_name = config.get('bucket_name')

    uploader = S3Uploader(aws_access_key_id, aws_secret_access_key, destination_folder, bucket_name)
    uploader.upload_file(args.file_path, bucket_name)
    print(f"File uploaded successfully to S3: {destination_folder}", file=sys.stderr)

if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        print(f"Error: {e}", file=sys.stderr)
        sys.exit(-1)


